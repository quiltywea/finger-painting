// Canvas code inspired by: https://github.com/rburchell/qmlstuff/blob/master/fingerpaint/fingerpaint.qml

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import Qt.labs.settings 1.0

import Utils 1.0
import "colors.js" as Colors
import "Components"

Page {
    id: page
    title: i18n.tr('Finger Painting')

    property string imageImport
    property string stamp
    property string text
    property var stamps: [
        { path: '../assets/smile.svg' },
        { path: '../assets/happy.svg' },
        { path: '../assets/happy2.svg' },
        { path: '../assets/heart-eyes.svg' },
        { path: '../assets/joy.svg' },
        { path: '../assets/sleep.svg' },
        { path: '../assets/sunglasses.svg' },
        { path: '../assets/surprise.svg' },
        { path: '../assets/tongue.svg' },
        { path: '../assets/wink.svg' },
        { path: '../assets/party.svg' },
        { path: '../assets/thumbsup.svg' },
        { path: '../assets/star.svg' },
        { path: '../assets/heart.svg' },
    ]

    function importImage(path) {
        if (page.imageImport) {
            Utils.remove(page.imageImport);
        }

        page.imageImport = path;
        canvas.loadImage(path);
    }

    Settings {
        id: settings

        property string background: '#000000'
        property int lineWidth: 5
        property bool randomize: true
        property string primaryColor: '#55FF55'
    }

    header: PageHeader {
        id: header
        title: parent.title

        trailingActionBar.actions: [
            Action {
                iconName: 'info'
                text: i18n.tr('About')
                onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'))
            },

            Action {
                iconName: 'save'
                text: i18n.tr('Save')
                onTriggered: {
                    var date = (new Date()).toISOString();
                    // TODO no hardcoding
                    var path = '/home/phablet/.cache/finger-painting.bhdouglass/' + date + '.png';
                    canvas.save(path);

                    PopupUtils.open(exportDialog, root, { 'path': path });
                }
            },

            Action {
                iconName: 'document-open'
                text: i18n.tr('Open')
                onTriggered: PopupUtils.open(importDialog, root)
            }
        ]
    }

    Rectangle {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            bottomMargin: bottomBar.height
        }
        color: settings.background

        Canvas {
            id: canvas

            anchors.fill: parent

            property var lastPosById
            property var posById

            property var colors: []

            function clear() {
                canvas.lastPosById = {};
                canvas.posById = {};
                canvas.context.reset();
                canvas.requestPaint();
            }

            Component.onCompleted: {
                for (var i = 0; i < 10; i++) {
                    colors.push(settings.primaryColor);
                }

                for (var i = 0; i < stamps.length; i++) {
                    canvas.loadImage(stamps[i].path);
                }
            }

            onImageLoaded: {
                if (imageImport) {
                    canvas.getContext('2d').drawImage(imageImport, 0, 0, canvas.width, canvas.height);
                    canvas.requestPaint();
                }
            }

            function paintLine() {
                var ctx = getContext('2d');
                if (!canvas.lastPosById) {
                    canvas.lastPosById = {};
                    canvas.posById = {};
                }

                for (var id in canvas.lastPosById) {
                    ctx.strokeStyle = colors[id % colors.length];
                    ctx.lineWidth = parseInt(settings.lineWidth);
                    ctx.lineCap = 'round';

                    ctx.beginPath();
                    ctx.moveTo(canvas.lastPosById[id].x, canvas.lastPosById[id].y);
                    ctx.lineTo(canvas.posById[id].x, canvas.posById[id].y);
                    ctx.stroke();

                    canvas.lastPosById[id] = canvas.posById[id];
                }
            }

            MultiPointTouchArea {
                anchors.fill: parent

                onPressed: {
                    if (!canvas.lastPosById) {
                        canvas.lastPosById = {};
                        canvas.posById = {};
                    }

                    var ctx = canvas.getContext('2d');
                    for (var i = 0; i < touchPoints.length; ++i) {
                        var point = touchPoints[i];

                        if (stamp) {
                            ctx.drawImage(
                                stamp,
                                point.x - 72,
                                point.y - 72
                            );
                        }
                        else if (text) {
                            if (settings.randomize) {
                                ctx.fillStyle = Colors.random(settings.background, canvas.colors[key]);
                            }
                            else {
                                ctx.fillStyle = settings.primaryColor;
                            }
                            ctx.font = (settings.lineWidth * 5) + 'px Ubuntu';
                            ctx.fillText(
                                text,
                                point.x - 144,
                                point.y - 36
                            );
                        }
                        else {
                            if (!canvas.lastPosById[point.pointId]) {
                                var key = (point.pointId % canvas.colors.length);
                                //Only randomize the last touch (so current touches don't change colors)
                                if (settings.randomize) {
                                    canvas.colors[key] = Colors.random(settings.background, canvas.colors[key]);
                                }
                                else {
                                    canvas.colors[key] = settings.primaryColor;
                                }
                            }

                            canvas.lastPosById[point.pointId] = {
                                x: point.x,
                                y: point.y
                            };

                            //Offset slightly to allow a single tap/dot
                            canvas.posById[point.pointId] = {
                                x: point.x + 1,
                                y: point.y + 1
                            };

                            canvas.paintLine();
                        }
                    }

                    canvas.requestPaint();
                }

                onUpdated: {
                    for (var i = 0; i < touchPoints.length; ++i) {
                        var point = touchPoints[i];

                        canvas.posById[point.pointId] = {
                            x: point.x,
                            y: point.y
                        };
                    }

                    canvas.paintLine();
                    canvas.requestPaint();
                }

                onReleased: {
                    canvas.requestPaint();

                    for (var i = 0; i < touchPoints.length; ++i) {
                        var point = touchPoints[i];

                        delete canvas.lastPosById[point.pointId];
                        delete canvas.posById[point.pointId];
                    }
                }
            }
        }
    }

    SubBottomBar {
        id: subBottomBar

        anchors {
            left: parent.left
            right: parent.right
            bottom: bottomBar.top
        }

        settings: settings
        mode: bottomBar.mode

        onStampChanged: parent.stamp = stamp
        onTextChanged: parent.text = text
    }

    BottomBar {
        id: bottomBar
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            bottomMargin: Qt.inputMethod.visible ? Qt.inputMethod.keyboardRectangle.height : 0
        }
    }

    Component {
        id: exportDialog
        ExportDialog {}
    }

    Component {
        id: importDialog
        ImportDialog {
            onImported: importImage(path)
        }
    }

    Connections {
        target: ContentHub

        onImportRequested: importImage(transfer.items[0].url);
        onShareRequested: importImage(transfer.items[0].url);
    }

    Component.onDestruction: {
        if (page.imageImport) {
            Utils.remove(page.imageImport);
        }
    }
}
